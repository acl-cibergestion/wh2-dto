package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoContabilizacion {

    private Long id;

    private EstadoContabilizacion estadoCont;

    private Date fechaContabilizacion;

    private BigDecimal numeroPcf;

    private Formalizacion formalizacion;
}
