package cl.workflow.cibergestion.wh2.dto.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenRequestTo {

    private String username;
    private String password;
    private String type;
    /** id_token jwt que se recibe de google o microsoft **/
    private String token;

}
