package cl.workflow.cibergestion.wh2.dto.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefreshTokenResponseTO {

    private String token;

    private final String tokenType = "Bearer";

    private String refreshToken;

    private Long expiredIn;

    public RefreshTokenResponseTO() {

    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpiredIn() {
        return expiredIn;
    }

    public void setExpiredIn(Long expiredIn) {
        this.expiredIn = expiredIn;
    }
}
