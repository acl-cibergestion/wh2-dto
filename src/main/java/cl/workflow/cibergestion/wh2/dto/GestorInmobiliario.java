package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GestorInmobiliario {

    private Long id;

    private String correo;

    private Boolean estado;

    private String nombre;

    private String nombreCorto;

    private Boolean prioridad;

    private String rut;

    private String telefono;

    private String telefono2;

    private Set<PersonaJuridica> wfhPersonasJuridicas;

}
