package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrNota {

    private Long id;

    private String descripcion;

    private TipoNota tipoNota;

    private Producto producto;

    private Usuario usuario;

}
