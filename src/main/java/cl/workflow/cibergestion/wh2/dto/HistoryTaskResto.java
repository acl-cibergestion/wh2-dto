package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryTaskResto {

    private String id;

    private int tEstandarD;

    private int tEstandarH;

    private int tRealD;

    private int tRealH;

    private String deleteReasonOld;

    private Carpeta carpeta;

    private Long carpetaId;

    private String detalleEtapa;

    private Rol rol;

    private int contador;

    private Date warningDueDate;

    private HistoryTask historyTask;

    private RunningTask runningTask;

}
