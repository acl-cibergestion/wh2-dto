package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Documento {

    private Long id;

    private Date fechaIngreso;

    private String nombre;

    private String observacion;

    private String ruta;

    private String estado;

    private Date fechaEliminacion;

    private TipoDocumento tipoDocumento;

    private String tipoDocumentoLibre;

    private Archivo archivo;

    private Carpeta carpeta;

    private Usuario usuario;

    private Set<Producto> productos;

}
