package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BrAdicional {
    private Long id;

    private BienRaiz bienRaiz;

    private TipoAdicional tipoAdicional;

    private String numero;

    private String rol;

    private DerechoAdicional derechoAdicional;

    private Set<Inscripcion> inscripciones;
}
