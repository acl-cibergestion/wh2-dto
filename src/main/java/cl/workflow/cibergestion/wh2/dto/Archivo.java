package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Archivo {

    private Long id;

    private byte[] data;

    private String extension;

    private Boolean imagenGop;

    private String nombre;

    private String tipo;

    private Set<Documento> wfhDocumentos;

}
