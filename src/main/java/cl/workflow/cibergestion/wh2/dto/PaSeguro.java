package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaSeguro {

    private Long id;

    private Date fechaAprob;

    private String numFolio;

    private String totalFinalSeguro;

    private BigDecimal totalFinalSeguroBD;

    private String porcPartSeguro;

    private Double sobreprima;

    private CaParticipante caParticipante;

    private Seguro seguro;

}
