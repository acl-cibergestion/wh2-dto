package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CiaSeguro {

    private Long id;

    private Boolean activo;

    private String direccion;

    private String nombre;

    private String nombreCorto;

    private String rut;

    private String telefono;

}
