package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarcaBanco {

    private Long id;

    private String descripcion;

    private Boolean activo;

    private Banco cgModBanco;

    private List<Sucursal> cgModSucursales;

}
