package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryProcessInstance {

    private String id;

    private HistoryProcessInstance runningExecutionProceso;

    private Carpeta carpeta;

    private String businessKey;

    private String procDefId;

    private Date startTime;

    private Date endTime;

    private Long duration;

    private String startUserId;

    private String startActId;

    private String endActId;

    private HistoryProcessInstance superProcessInstanceId;

    private List<HistoryProcessInstance> superProcessInstanceHijas;

    private String deleteReason;

    private String tenantId;

    private String name;

    private List<HistoryProcessInstance> runningExecutions;

}
