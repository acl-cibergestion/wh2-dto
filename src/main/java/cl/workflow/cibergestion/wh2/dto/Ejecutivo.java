package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ejecutivo {

    private Long id;

    private TipoEjecutivo tipoEjecutivo;

    private String nombres;

    private String apPaterno;

    private String apMaterno;

    private String email;

    private Boolean estado;

    private Institucion institucion;

    private String nombreCreador;

    private String nombre2;

    private String rut;

    private Sucursal sucursal;

    private String telefono1;

    private String telefono2;

    private String codigoEjecutivo;

    private String nombreCompleto;

}
