package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Banco {

    private Long id;

    private String nombre;

    private Boolean activo;

    private List<MarcaBanco> cgModMarcasBancos;

    private List<Sucursal> cgModSucursales;

}
