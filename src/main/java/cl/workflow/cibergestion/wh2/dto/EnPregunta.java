package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnPregunta {

    private Long id;

    private String evaluado;

    private String pregunta;

    private Boolean vigente;

    private Encuesta encuesta;

    private Set<FoEnRespuesta> respuestas;

}
