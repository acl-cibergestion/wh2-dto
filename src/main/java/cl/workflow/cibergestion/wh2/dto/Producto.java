package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Producto {

    private Long id;

    private FinalidadCredito finalidad;

    private Boolean activo;

    private String nombreFantasia;

    private TipoProducto tipoProducto;

    private Moneda moneda;

    private PeriodicidadPago periodicidadPago;

    private String nombre;

    private TipoTasa tipoTasa;

    private Integer anioRenovacionTasa;

    private String codigo;

    private String nombreCorto;

    private Double financiamientoMaximo;

    private Integer aniosTasaFija;

    private Date fechaRegistro;

    private Institucion institucion;

    private Boolean mesExclusion;

    private Integer mesGracia;

    private Integer plazoTasaAlternativa;

    private String plazos;

    private Double tasa;

    private Set<CaCredito> creditos;

    private Set<Documento> documentos;

    private Producto productoComplementario;

    private Set<Producto> productos;

    private Set<PrNota> notas;

    private Set<PrTasa> tasas;

    private Set<Seguro> seguros;

}
