package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunningExecution {

    private String id;

    private Long rev;

    private String businessKey;

    private RunningExecution parentId;

    private List<RunningExecution> executionHijas;

    private String procDefId;

    private RunningExecution superExec;

    private RunningExecution superExecutionHija;

    private String actId;

    private int isActive;

    private int isConcurrent;

    private int isScope;

    private int isEventScope;

    private Long suspensionState;

    private Long cachedEntState;

    private String tenantId;

    private String name;

    private Date lockTime;

    private Carpeta carpeta;

    private RunningExecution runningExecutionProceso;

    private List<RunningExecution> runningExecutions;

    private RunningTask runningTask;

    private List<RunningTask> runningTasks;

    private List<RunningIdentityLink> runningIdentityLinks;

    private List<RunningVariable> runningVariables;

}
