package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoOperacion {

    private Long id;

    private BigDecimal activo;

    private String descripcion;

    private Set<Carpeta> carpetas;

    private Set<CaCredito> wfhCaCreditos;

    private Set<Flujo> flujos;

}
