package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Intermediario {

    private Long id;

    private Boolean activo;

    private String descripcion;

    private String rut;

    private Set<Seguro> wfhSeguros;

}
