package cl.workflow.cibergestion.wh2.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseTo {

    private String username;
    private String firstName;
    private String secondName;
    private String apellidoP;
    private String apellidoM;
    private String cargo;
    private String email;
    private String rut;
    private String sucursal;
    private String telefono;

}
