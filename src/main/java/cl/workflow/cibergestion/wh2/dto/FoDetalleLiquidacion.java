package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoDetalleLiquidacion {

    private Long id;

    private Banco banco;

    private BigDecimal monto;

    private BigDecimal numero;

    private TipoLiquidacion tipoLiquidacion;

    private Formalizacion formalizacion;
}
