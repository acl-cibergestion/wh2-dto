package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Perfil {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Boolean perfilMod;

    private Boolean perfilOpe;

    private String permiso;

    private String permisoDescripcion;

}
