package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.SortedSet;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaJuridica {

    private Long id;

    private String rut;

    private Set<CaParticipante> caParticipantes;

    private SortedSet<PjProyecto> pjProyectos;

    private String celular1;

    private String correo;

    private Boolean estado;

    private String nombre;

    private String nombreCorto;

    private Boolean prioridad;

    private String telefono1;

    private String telefono2;

    private TipoPersona tipoPersona;

    private GestorInmobiliario gestorInmobiliario;

}
