package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaParticipante {

    private Long id;

    private Long tipoParticipanteId;

    private TipoParticipante tipoParticipante;

    private Persona persona;

    private PersonaJuridica personaJuridica;

    private Set<PaSeguro> paSeguros;

    private Set<PaDireccion> paDirecciones;

    private Actividad actividad;

    private String apellidoMaterno;

    private String apellidoPaterno;

    private Boolean art150;

    private Boolean beneficiario;

    private String celular1;

    private String celular2;

    private String correo;

    private EstadoCivil estadoCivil;

    private EstadoDps estadoDps;

    private Date fechaDps;

    private Date fechaFirmaNotaria;

    private Date fechaIngresoFirmaNotaria;

    private Genero genero;

    private MotivoNoContacto motivoNoFirma;

    private Boolean indFirmaNotaria;

    private LeyBeneficioTributario leyBenefTributario;

    private BigDecimal montoUfBenef;

    private Nacionalidad nacionalidad;

    private String nombres;

    private BigDecimal porcPartCred;

    private BigDecimal porcParticBienraiz;

    private Profesion profesion;

    private RegimenMatrimonial regimen;

    private String telefono1;

    private String telefono2;

    private TipoPersona tipoPersona;

    private TipoSolicitante tipoSolicitante;

    private BigDecimal totalTasasSeguros;

    private Carpeta carpeta;

    private CaParticipante conyuge;

    private CaParticipante conyugeRef;

    private Usuario wfhUsuario;

    private Boolean indParticipaConyuge;

    String tipoIdentificacion;

    private String identificacion;

    private BigDecimal partSegDesg;

    private BigDecimal tasaSegDesg;

    private BigDecimal tasaOtrosSeg;

}
