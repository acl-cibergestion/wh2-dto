package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaDireccion {

    private Long id;

    private CaParticipante caParticipante;

    private String calle;

    private Comuna comuna;

    private String depto;

    private Character eCorrespondencia;

    private TipoDireccion tipo;

    private String numCasilla;

    private String numero;

    private String resto;
}
