package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunningVariable {

    private String id;

    private Long rev;

    private String type;

    private String name;

    private RunningExecution executionId;

    private String processInstanceId;

    private String taskId;

    private String byteArrayId;

    private Double double_;

    private Long long_;

    private String text_;

    private String text2_;

}
