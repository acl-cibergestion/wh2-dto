package cl.workflow.cibergestion.wh2.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordRequestTo {
    private String username;
    private String lastPassword;
    private String newPassword;
    private String token;
}
