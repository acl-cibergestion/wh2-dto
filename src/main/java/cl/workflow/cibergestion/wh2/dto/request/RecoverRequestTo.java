package cl.workflow.cibergestion.wh2.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoverRequestTo {
    String email;
}
