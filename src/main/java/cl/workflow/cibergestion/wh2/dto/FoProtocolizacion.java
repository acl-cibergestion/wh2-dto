package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoProtocolizacion {

    private Long id;

    private Ciudad ciudad;

    private Date fechaEscritura;

    private BigDecimal nroRepertorio;

    private TipoEscritura tipoEscritura;

    private Set<CaCredito> wfhCaCreditos;

    private Formalizacion formalizacion;

    private Usuario notaria;

}
