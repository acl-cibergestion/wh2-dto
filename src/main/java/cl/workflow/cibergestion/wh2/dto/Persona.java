package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Persona {

    private Long id;

    private String apellido1;

    private String apellido2;

    private Date fechaNacimiento;

    private Nacionalidad nacionalidad;

    private String nombres;

    private String pasaporte;

    private String rut;

    private Genero sexo;

    private String telefonoPart;

    private String tipoIdentificador;

    private TipoPersona tipoPersona;

}
