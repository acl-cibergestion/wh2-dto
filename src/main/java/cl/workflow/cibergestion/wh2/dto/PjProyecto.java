package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PjProyecto {

    private Long id;

    private PersonaJuridica personaJuridica;

    private Comuna comuna;

    private String calle;

    private String numero;

    private String resto;

    private String nombre;

    private TipoPagoInmobiliaria tipoPagoInmobiliaria;

    private Boolean estado;

    private ConvenioInmobiliaria convenioInmobiliaria;

    private Date fechaRecepcFinal;

    private Date fechaTasacion;

    private Date fechaTitulos;

    private String nombreCorto;

    private String numOpInst;

    private Long numPrioridad;

    private String rolMatriz;

    //private Set<BienRaiz> wfhBienesRaices;

    // email auxiliares
    private String email1;
    private String email2;
    private String email3;

}
