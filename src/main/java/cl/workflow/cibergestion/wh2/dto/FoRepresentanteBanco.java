package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoRepresentanteBanco {

    private Long id;

    private Date fechaFirma;

    private Boolean flagFirma;

    private MotivoNoContacto motivoNoFirma;

    private RepresentanteBanco representante;

    private Formalizacion formalizacion;

}
