package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.SortedSet;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Checklist {

    private Long id;

    private String codigoActiviti;

    private String nombre;

    private String tipoBloque;

    private String envioCorreo;

    private SortedSet<ChOpcion> opciones;

    private Set<FoChecklistResponsable> wfhFoChecklistsResponsables;
}
