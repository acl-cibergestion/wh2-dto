package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaEliminada {

    private Long id;

    private Date fechaEliminacion;

    private MotivoEliminacion motivo;

    private Carpeta carpeta;

    private Usuario responsable;

}
