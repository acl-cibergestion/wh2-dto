package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Seguro {

    private Long id;

    private String glosa;

    private CiaSeguro ciaSeguro;

    private String poliza;

    private BigDecimal tasa;

    private TipoSeguro tipoSeguro;

    private Boolean vigente;

    private Banco banco;

    private String cobertura;

    private Boolean colectivo;

    private Date desde;

    private Boolean enSimulador;

    private String glosaCorta;

    private Date hasta;

    private Institucion institucion;

    private String nombreSimulador;

    private PeriodicidadPago periodicidad;

    private BigDecimal tasaIncendio;

    private BigDecimal tasaSismo;

    private String tipoCobertura;

    /*private Set<BienRaiz> bienesRaices;*/

    private Set<PaSeguro> seguros;

    private Intermediario intermediario;

    private Set<Producto> productos;

    private Set<Seguro> seAsociados;

    private Set<Seguro> seAsociadosRef;

}
