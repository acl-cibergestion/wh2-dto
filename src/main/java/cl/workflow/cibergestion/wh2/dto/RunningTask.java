package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunningTask {

    private String id;

    private Long rev;

    private RunningExecution runningExecution;

    private RunningExecution runningExecutionProceso;

    private String procDefId;

    private String name;

    private String parentTaskId;

    private String description;

    private String taskDefKey;

    private String owner;

    private Usuario usuario;

    private String delegation;

    private Long priority;

    private Date createTime;

    private Date dueDate;

    private String category;

    private Long suspensionState;

    private String tenantId;

    private String formKey;

    private HistoryTaskResto historyTaskResto;

}
