package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoAutenticacion {

    private Long id;

    private Integer codigo;

    private String nombre;

    private boolean habilitado;

    private String descripcion;

    private Date fechaCreado;

    private Date fechaActualizacion;
}
