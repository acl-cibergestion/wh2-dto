package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Formalizacion {

    private Long id;

    private Usuario abogadoEscriturador;

    private Usuario abogadoTitulos;

    private Gestoria gestoria;

    private ConvenioEmpleado convenioEmpleado;

    private Date fmac;

    private ClausulaPrepago clausulaPrepago;

    private BigDecimal valorClausulaPrepago;

    private Usuario notaria;

    private BigDecimal sumaCreditosEnUf;

    private BigDecimal sumaCreditosEnPesos;

    private FormaPago formaPago;

    private EstadoCtaTrj estadoCtaTrj;

    private Long cartaResguardo;

    private Boolean caucionada;

    private Boolean indFirmaNotaria;

    private Ciudad ciudadFirma;

    private String datosComplementarios;

    private BigDecimal diaCargo;

    private Usuario ejecutivoCiber;

    private Ejecutivo ejecutivoControl;

    private Ejecutivo ejecutivoNegocio;

    private Boolean envioEncuesta;

    private ExcepcionCurse excepcionCurse;

    private Date fechaCostoFondo;

    private Date fechaLimite;

    private Date fechaLiquidacion;

    private Date fechaAprobacion;

    private Date fechaCalculo;

    private Date fechaEscritura;

    private Boolean firmaCheckIngresador;

    private Boolean firmaCheckVisado;

    private Institucion institucion;

    private BigDecimal montoCredito;

    private String nroRepertorio;

    private BigDecimal nroOtChileexpress;

    private String numCtaCte;

    private String obsLiqPago;

    private String observaciones;

    private Segmento segmentoAtencion;

    private Sucursal sucursal;

    private Sucursal sucursalProveedor;

    private TipoEscritura tipoEscritura;

    private Long tipoOperacionId;

    private Usuario usuario;

    private EstadoVisacion estadoVisacion;

    private Carpeta carpeta;

    /*private FoContabilizacion foContabilizacion;

    private Set<FoChatVisado> wfhFoChatsVisados;

    private Set<FoChecklist> wfhFoChecklists;

    private Set<FoChecklistResponsable> wfhFoChecklistsResponsables;

    private Set<FoClausula> clausulas;

    private SortedSet<FoDesarrolloDeuda> foDesarrollosDeuda;

    private Set<FoDetalleLiquidacion> detallesLiquidacion;

    private Set<FoDocSolTitulo> wfhFoDocSolTitulos;

    private Set<FoGastoOperacional> gastosOperacionales;

    private SortedSet<FoInstruccionPago> instruccionesPago;

    private Set<FoProtocolizacion> foProtocolizaciones;

    private SortedSet<FoRepresentanteBanco> representantesBancoNotaria;*/

}
