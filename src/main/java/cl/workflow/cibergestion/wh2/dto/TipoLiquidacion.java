package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoLiquidacion {

    private Long id;

    private String descripcion;

    private TipoDetLiquidacion tipoDetLiquidacion;

    private Boolean activo;
}
