package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario {

    private Long id;

    private Institucion institucion;

    private List<Rol> roles;

    private String primerNombre;

    private String apellidoPaterno;

    private Boolean activo;

    private Boolean adminSistema;

    private Boolean susario;

    private String apellidoMaterno;

    private String cargo;

    private Cargo cargoUsuario;

    private String clave;

    private String email;

    private Date fechaCambioClave;

    private Integer loginAttempts;

    private Boolean loginAutoLock;

    private String loginAutoLockToken;

    private String nombreUsuario;

    private String perfiles;

    private Date recoveryDateExp;

    private String recoveryToken;

    private String rut;

    private String segundoNombre;

    private String sucursalResto;

    //private Sucursal sucursal;

    private Boolean hasSupervisor;

    private Boolean superUsuario;

    private String telefono;

    private Boolean tempAccess;

    private String tempPassw;

    private Date tempPasswExp;

    private Boolean eliminado;

    private Usuario supervisor;

    private Set<Usuario> supervisados;

    private List<Perfil> perfilList;

    private List<Perfil> perfilListFolder;

    private List<Perfil> perfilListModules;

}