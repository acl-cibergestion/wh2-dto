package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaCredito {

    private Long id;

    private Producto producto;

    private FinalidadCredito finalidad;

    private String numSolicitud;

    private String numDocumento;

    private BigDecimal comision;

    private Carpeta carpeta;

    private Integer plazo;

    private BigDecimal costoFondo;

    private BigDecimal spread;

    private BigDecimal tasaInteres;

    private Integer numCuotas;

    private Integer mesesGracia;

    private List<String> mesesExclusion;

    private BigDecimal precioVentaUf;

    private BigDecimal precioVentaPesos;

    private BigDecimal montoCreditoUf;

    private BigDecimal montoCreditoPesos;

    private LeyCertificacion leyCertif;

    private BigDecimal montoContadoUf;

    private BigDecimal montoContadoPesos;

    private TasaImpuestoMutuo tasaImpMutuo;

    private BigDecimal gastoUfImplMutuo;

    private BigDecimal relacionDeudaGarantia;

    private String numOperacion;

    private BigDecimal montoBenefUf;

    private BigDecimal montoGopUf;

    private String gopEnCredito;

    private BigDecimal montoResguardo;

    private BigDecimal montoUfSubs;

    private BigDecimal montoAhorroUfSubs;

    private BigDecimal montoPesosSubs;

    private Boolean flagSubsidio;

    private Banco instAhorroSubs;

    private TipoSubsidio tipoSubs;

    private BigDecimal totalSubsAhorroUf;

    private Date fechaVencimiento;

    private String numCredPrepago1;

    private String numCredPrepago2;

    private String numCredPrepago3;

    private List<Producto> productosDisponibles;

    private DestinoCredito destino;

    private BigDecimal impMutuo;

    private FoProtocolizacion wfhFoProtocolizacione;

    private TipoOperacion wfhTiposOperacion;

    private Usuario wfhUsuario1;

    private Usuario wfhUsuario2;

}
