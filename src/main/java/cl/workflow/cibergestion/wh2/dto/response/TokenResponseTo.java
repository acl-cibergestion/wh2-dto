package cl.workflow.cibergestion.wh2.dto.response;

import cl.workflow.cibergestion.wh2.dto.Rol;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Getter
@Setter
public class TokenResponseTo {

    private String token;

    private String nombreUsuario;

    private String correo;

    private List<Rol> rol;

    private String tipoAutenticacion;

    private List<GrantedAuthority> listGrantedAuthority;

    private String refreshToken;

    private Long expiredIn;

    private Boolean expiredPassword;

    private String primerNombre;

    private String segundoNombre;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private String cargo;

    private String rut;

    private String sucursal;

    private String telefono;

}
