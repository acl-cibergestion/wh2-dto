package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BrDatoAlzamiento {

    private Long id;

    private BienRaiz bienRaiz;

    private Boolean tipoAlzamiento;

    private Boolean sinacofi;

    private Banco bancoAcreedor;

    private Boolean cartaResguardo;

    private String contacto;

    private String sucursal;

    private String nisn;

    private String nnse;

    private String telefono;

    private Boolean beneficiario;

    private Date fechaFirmaBancoAcreedor;

    private Boolean firmoBancoAcreedor;

    private BigDecimal montoCartaResguardoUf;

    private BigDecimal montoReparoUf;

    private MotivoNoContacto motivoNoFirma;
}
