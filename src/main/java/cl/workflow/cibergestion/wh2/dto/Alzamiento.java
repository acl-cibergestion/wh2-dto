package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Alzamiento {

    private Long id;

    private Segmento segmentoAtencion;

    private String datosComplementarios;

    private Usuario responsable;

    private Carpeta carpeta;

}
