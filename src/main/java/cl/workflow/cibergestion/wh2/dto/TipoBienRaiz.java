package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoBienRaiz {

    private Long id;

    private String nombre;

    private BigDecimal porcentaje;

    private TipoTasacion tipoTasacion;

    private Boolean activo;
}
