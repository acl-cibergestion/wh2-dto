package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ciudad {

    private Long id;

    private BigDecimal estadoCambioCod;

    private String nombre;

    private Region region;

    private Set<Comuna> comunas;

    private Boolean activo;
}
