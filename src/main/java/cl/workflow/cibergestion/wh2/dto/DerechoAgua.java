package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DerechoAgua {

    private Long id;


    private Comuna cbr;

    private String nombre;


    private BienRaiz bienRaiz;


    private Set<Inscripcion> inscripciones;
}
