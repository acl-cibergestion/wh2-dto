package cl.workflow.cibergestion.wh2.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrTasa {

    private Long id;

    private BigDecimal comision;

    private Integer desde;

    private Date fecha;

    private Integer hasta;

    private BigDecimal montoDesde;

    private BigDecimal montoHasta;

    private BigDecimal valor;

    private Producto producto;

    private Usuario usuario;

}
