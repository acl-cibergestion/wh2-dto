package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inscripcion {

    private Long id;

    private BigDecimal anio;

    private BigDecimal fojas;

    private GradoInscripcionHipoteca grado;

    private BigDecimal numero;

    private TipoGarantia tipoGarantia;

    private BienRaiz bienRaiz;

    private BrAdicional adicional;

    private DerechoAgua derechoAgua;

    private TipoInscripcion tipoInscripcion;
}
