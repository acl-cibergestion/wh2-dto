package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryTask {

    private String id;

    private String taskDefKey;

    private String procDefKey;

    private String procDefId;

    private String rootProcInstId;

    private HistoryProcessInstance executionProceso;

    private String procInstanceId;

    private String execution;

    private String caseDefKey;

    private String caseDefId;

    private String caseInstId;

    private String caseExecutionId;

    private String actInstId;

    private String name;

    private String parentTaskId;

    private String description;

    private String owner;

    private Usuario usuario;

    private String assignee;

    private Date startTime;

    private Date endTime;

    private Long duration;

    private String deleteReason;

    private Long priority;

    private Date dueDate;

    private Date followUpDate;

    private String tenantId;

    private Date createTime;

    private HistoryTaskResto historyTaskResto;

}
