package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoInstruccionPago {

    private Long id;

    private String banco;

    private String bancoSaldo;

    private String cliente;

    private String cuentaCorriente;

    private String cuentaCorrienteSaldo;

    private DestinoCredito destinoCredito;

    private String ejecutivoSaldo;

    private Date fecha;

    private Moneda moneda;

    private BigDecimal montoCredito;

    private BigDecimal numeroPcf;

    private String observacion;

    private String opHipo1;

    private String opHipo2;

    private String opHipo3;

    private OportunidadPago oportunidadPago;

    private String opPrep1;

    private String opPrep2;

    private String opPrep3;

    private String otro;

    private PagoPrincipal pagoPrincipal;

    private String rut;

    private String rutVendedor;

    private SaldoCredito saldoCredito;

    private String sucursalSaldo;

    private Canal canal;

    private Formalizacion formalizacion;

    private Usuario modificador;

    private String tareaActual;

    private String bcoAcr1;

    private String bcoAcr2;

    private String bcoAcr3;

}
