package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoTasacion {

    private Long id;

    private BigDecimal factorGeneral;

    private BigDecimal factorVivienda;

    private String nombre;

    private BigDecimal tarifaMaxima;

    private BigDecimal tarifaMinima;

    private Boolean activo;

    private List<TipoBienRaiz> tiposBienes;
}
