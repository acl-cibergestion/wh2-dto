package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunningIdentityLink {

    private String id;

    private Long rev;

    private String groupId;

    private String type;

    private String userId;

    private String taskId;

    private RunningExecution processInstanceId;

    private String processDefinitionId;

}
