package cl.workflow.cibergestion.wh2.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoverResponseTo {
    String email;
    String name;
    String token;
    String dateExp;
    String timeExp;
}
