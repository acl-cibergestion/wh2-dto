package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Region {

    private Long id;

    private String nombre;

    private BigDecimal orden;

    private Boolean activo;

    private Set<Ciudad> ciudades;

}
