package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BienRaiz {

    private Long id;

    private Set<Carpeta> carpetas;

    private SortedSet<BrDatoAlzamiento> datosAlzamiento;

    private String calle;

    private String numero;

    private Region region;

    private Ciudad ciudad;

    private Comuna comuna;

    private String numDepto;

    private String resto;

    private String numRol;

    private Boolean flagEncadenamiento;

    private TipoBienRaiz tipoBienRaiz;

    private BrEstado estadoBienRaiz;

    private TipoPagoInmobiliaria tipoPagoInmobiliaria;

    private Date fechaTitulos;

    private Date fechaGgpp;

    private String ubicacionReferencia;

    private String contactoNombre;

    private String contactoTelefono;

    private String contactoObserv;

    private String numSolicTasacion;

    private Date fechaTasacion;

    private Tasador tasador;

    private EstadoTasacion estadoTasacion;

    private Boolean mercadoObjeto;

    private BigDecimal superfTerreno;

    private TipoConstruccion tipoConstruccion;

    private Integer anioConstruccion;

    private BigDecimal superfConst;

    private UnidadMedidaTerreno unidad;

    private Integer indicadorViviendaSocial;

    private Boolean dfl2;

    private Integer numPropiedadDFL2;

    private Moneda monedaPactada;

    private BigDecimal valorTasacionuf;

    private Boolean selloVerde;

    private BigDecimal valorLiquidezuf;

    private BigDecimal montoMinAsegurableuf;

    private String observacionTasacion;

    private String numBienraizInst;

    private String numGtiaInst;

    private TipoGarantia tipoGarantia;

    private String afavorde;

    private BigDecimal annoCertifAsigRoles;

    private String certifAsigRoles;

    private BigDecimal certificadoNro;

    private Date fechaPermisoEdif;

    private Date fechaRecepcFinal;

    private Boolean matriceriaHipo;

    private Comuna municipalidad;

    private String numCaratulaCbr;

    private Date fechaEstimadaSalidaCBR;

    private BigDecimal totalMontoUfCresg;

    private BigDecimal totalMontoUfReparo;

    private BigDecimal valorTasacionpesos;

    private Seguro seguro;

    private Usuario wfhUsuario;

    private DerechoAgua derechoAgua;

    private Set<Inscripcion> inscripciones;

    private String direccion;
}
