package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Flujo {

    private Long id;

    private String descripcion;

    private String procDefId;

    private Set<Carpeta> carpetas;

    private Set<Canal> canales;

    private Set<TipoOperacion> tiposOperacion;
}
