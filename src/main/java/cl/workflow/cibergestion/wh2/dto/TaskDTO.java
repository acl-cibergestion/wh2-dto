package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO {

    private String idTask;

    private String idExecution;

    private String idHistory;

    private Carpeta carpeta;

    private String procDefId;

    private String actId;

    private String actNom;

    private Long priority;

    private String formId;

    private String checkId;

    private String responsable;

    private Date createTime;

    private Date dueDate;

    private String responsableNombre;

    private Rol rol;

    private String tree;
}
