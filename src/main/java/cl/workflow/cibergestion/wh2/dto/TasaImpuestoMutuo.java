package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TasaImpuestoMutuo {

    private Long id;

    private String valor;

    private Boolean activo;

}
