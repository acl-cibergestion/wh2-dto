package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sucursal {

    private Long id;

    private String glosa;

    private String descripcion;

    private Banco cgModBanco;

    private MarcaBanco marcaBanco;

    private TipoSucursal tipoSucursal;

    private Region region;

    private Institucion institucion;

    private Zona zona;

    private Boolean activo;
}
