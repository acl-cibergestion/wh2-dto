package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Titulo {

    private Long id;

    private Carpeta carpeta;

    private EstadoTitulo estadoTitulo;

    private PjProyecto proyecto;

    private Sucursal sucursalProveedor;

    private Usuario especialista;

    private Sucursal sucursalBanco;

    private Ejecutivo ejecutivoControl;

    private Ejecutivo ejecutivoNegocio;

    private Ejecutivo ejecutivoHipotecario;

    private Usuario abogadoTitulos;

}
