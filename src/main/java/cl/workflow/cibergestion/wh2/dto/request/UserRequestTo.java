package cl.workflow.cibergestion.wh2.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequestTo {

    private String actualUsername;
    private String newUsername;
    private String firstName;
    private String secondName;
    private String apellidoP;
    private String apellidoM;
    private String cargo;
    private String email;
    private String rut;
    private String sucursal;
    private String telefono;

}
