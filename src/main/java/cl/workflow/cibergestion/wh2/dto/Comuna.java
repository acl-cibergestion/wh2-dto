package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comuna {

    private Long id;

    private String nombre;

    private Boolean activo;

    private String cbrDescripcion;

    private Ciudad ciudad;

}
