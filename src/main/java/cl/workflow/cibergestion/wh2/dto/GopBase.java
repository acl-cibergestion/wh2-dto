package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GopBase {

    private Long id;

    private String descripcion;

    private Set<FoGastoOperacional> wfhFoGastosOperacionales;

    private Set<Gop> wfhGops;
}
