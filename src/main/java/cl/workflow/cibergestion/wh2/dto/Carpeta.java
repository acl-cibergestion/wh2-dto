package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Carpeta {

    private Long id;

    private EstadoCarpeta estadoCarpeta;

    private Date fechaIngreso;

    private Formalizacion formalizacion;

    private Alzamiento alzamiento;

    private Titulo titulo;

    private Flujo flujo;

    private Canal canal;

    private Carpeta carpetaPadre;

    private Set<Carpeta> carpetasHijas;

    private TipoOperacion tipoOperacion;

    private Usuario usuario;

    private TipoServicio servicioProveedor;

    private Institucion institucion;

    private Region region;

    private HistoryTaskResto historyTaskResto;

    private Set<CaCheckList> checkLists;

    private Set<CaCredito> creditos;

    private Set<CaEliminada> caEliminadas;

    private CaExcepcion caExcepcion;

    private Set<CaParticipante> participantes;

    private Set<Documento> documentos;

    private Set<FoEnRespuesta> respuestas;

    private Long prioridad;

    private MotivoPrioridad motivoPrioridad;

    private Sucursal sucursalBanco;

    private Boolean excepcion;

}
