package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChOpcion {

    private Long id;

    private String variableActiviti;

    private String nombre;

    private Boolean separador;

    private String tipo;

    private Set<FoChecklist> foChecklists;

    private String separadorStyle;
}
