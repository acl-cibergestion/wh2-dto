package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoDesarrolloDeuda {

    private Long id;

    private BigDecimal amortizacion;

    private BigDecimal dividendoSinSeguro;

    private String fechaVencimientoDiv;

    private BigDecimal intereses;

    private BigDecimal montoDividendo;

    private BigDecimal montoSeguro;

    private BigDecimal nven;

    private BigDecimal primaSegDesgravamen;

    private BigDecimal saldo;

    private BigDecimal tasaCae;

    private Formalizacion formalizacion;
}
