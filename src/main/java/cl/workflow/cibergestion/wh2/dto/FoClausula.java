package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoClausula {

    private Long id;

    private Clausula clausula;

    private ValorClausula valorClausula;

    private Boolean obligatorio;

    private Formalizacion formalizacion;
}
