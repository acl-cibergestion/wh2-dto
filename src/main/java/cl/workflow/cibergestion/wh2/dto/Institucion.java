package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Institucion {

    private Long id;

    private String razonSocial;

    private TipoInstitucion tipoInstitucion;

    private Boolean activo;

}
