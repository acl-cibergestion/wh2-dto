package cl.workflow.cibergestion.wh2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoEnRespuesta {

    private Long id;

    private BigDecimal valor;

    private Carpeta carpeta;

    private EnPregunta wfhEnPregunta;
}
